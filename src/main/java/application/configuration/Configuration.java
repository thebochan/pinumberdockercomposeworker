package application.configuration;

import application.Calculation.Calculation;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public BlockingQueue<Double> queue(){
        return new ArrayBlockingQueue<Double>(10);
    }

    @Bean
    public RouterFunction<ServerResponse> route(Calculation piHandler) {

        return RouterFunctions
                .route(RequestPredicates.GET("/pi").and(RequestPredicates.accept(MediaType.TEXT_PLAIN)), piHandler::calculate);
    }
}
