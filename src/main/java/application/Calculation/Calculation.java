package application.Calculation;

import lombok.Setter;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Setter
@Service
public class Calculation {

    private CompletableFuture<Double> result;
    private BlockingQueue<Double> queue;

    Calculation(BlockingQueue<Double> queue){
        this.queue = queue;
    }

    private Producer producer;
    private Consumer consumer;
//    Producer producer = new Producer( queue, 1000L);
//    Consumer consumer = new Consumer(queue, result);

    public Mono<ServerResponse> calculate(ServerRequest request) {
        try {
            String n = request.queryParam("n").get();
            result = new CompletableFuture<>();
            producer = new Producer(queue, Long.valueOf(n));
            consumer = new Consumer(queue, result);

            producer.start();
            consumer.start();


            Double resultNew = result.get() * 4;

            return ServerResponse.ok().contentType(MediaType.TEXT_PLAIN)
                    .body(BodyInserters.fromObject("pi = " + resultNew));
        } catch (InterruptedException e){
            return ServerResponse.ok().contentType(MediaType.TEXT_PLAIN)
                    .body(BodyInserters.fromObject("pi = " + "error"));
        } catch (ExecutionException e1){
            return ServerResponse.ok().contentType(MediaType.TEXT_PLAIN)
                    .body(BodyInserters.fromObject("pi = " + "error"));
        }
    }
}